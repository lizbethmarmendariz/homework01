/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ShoolControl;

import ShoolControl.Exams;
import java.awt.Image;
import java.awt.Toolkit;
import javax.swing.JOptionPane;

/**
 *
 * @author alumno
 */
public class ExamPartial3 extends javax.swing.JFrame {

    /**
     * Creates new form ExamParti
     */
    public ExamPartial3() {
        initComponents();
    }

    public Image getIconImage() {
        Image retValue = Toolkit.getDefaultToolkit().
                getImage(ClassLoader.getSystemResource("Images/exam.png"));
        return retValue;
    }
    
    public int i = 0;

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        Pregunta1 = new javax.swing.ButtonGroup();
        Pregunta2 = new javax.swing.ButtonGroup();
        Pregunta3 = new javax.swing.ButtonGroup();
        Pregunta4 = new javax.swing.ButtonGroup();
        Pregunta5 = new javax.swing.ButtonGroup();
        jPanel1 = new javax.swing.JPanel();
        Correcta4 = new javax.swing.JRadioButton();
        jLabel5 = new javax.swing.JLabel();
        PrimerIncorrecta4 = new javax.swing.JRadioButton();
        Correcta1 = new javax.swing.JRadioButton();
        SegundaIncorrecta4 = new javax.swing.JRadioButton();
        PrimerIncorrecta1 = new javax.swing.JRadioButton();
        Correcta5 = new javax.swing.JRadioButton();
        SegundaIncorrecta1 = new javax.swing.JRadioButton();
        PrimerIncorrecta5 = new javax.swing.JRadioButton();
        Correcta2 = new javax.swing.JRadioButton();
        SegundaIncorrecta5 = new javax.swing.JRadioButton();
        PrimerIncorrecta2 = new javax.swing.JRadioButton();
        SegundaIncorrecta2 = new javax.swing.JRadioButton();
        jLabel1 = new javax.swing.JLabel();
        Correcta3 = new javax.swing.JRadioButton();
        jLabel2 = new javax.swing.JLabel();
        PrimerIncorrecta3 = new javax.swing.JRadioButton();
        jLabel3 = new javax.swing.JLabel();
        SegundaIncorrecta3 = new javax.swing.JRadioButton();
        jLabel4 = new javax.swing.JLabel();
        jButton1 = new javax.swing.JButton();
        jButton2 = new javax.swing.JButton();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setTitle("Exam Partial 3");
        setIconImage(getIconImage());
        setMaximumSize(new java.awt.Dimension(1095, 705));
        setMinimumSize(new java.awt.Dimension(1095, 705));
        setPreferredSize(new java.awt.Dimension(1095, 705));

        jPanel1.setBackground(new java.awt.Color(255, 255, 255));
        jPanel1.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "Exam Partial 3", javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Copperplate Gothic Light", 0, 24))); // NOI18N
        jPanel1.setToolTipText("");
        jPanel1.setPreferredSize(new java.awt.Dimension(1095, 705));
        jPanel1.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        Correcta4.setBackground(new java.awt.Color(255, 255, 255));
        Pregunta4.add(Correcta4);
        Correcta4.setText("It shows only necessary details for an object, not the inner details of an object. ");
        jPanel1.add(Correcta4, new org.netbeans.lib.awtextra.AbsoluteConstraints(41, 393, -1, -1));

        jLabel5.setText("What is sealed modifiers?");
        jPanel1.add(jLabel5, new org.netbeans.lib.awtextra.AbsoluteConstraints(27, 486, -1, -1));

        PrimerIncorrecta4.setBackground(new java.awt.Color(255, 255, 255));
        Pregunta4.add(PrimerIncorrecta4);
        PrimerIncorrecta4.setText("It is a technique used by the compilers and instructs to insert complete body of the function wherever that function is used in the program source code.");
        jPanel1.add(PrimerIncorrecta4, new org.netbeans.lib.awtextra.AbsoluteConstraints(41, 418, -1, -1));

        Correcta1.setBackground(new java.awt.Color(255, 255, 255));
        Pregunta1.add(Correcta1);
        Correcta1.setText("It is a collection of abstract method. If the class implements an inheritance, and then thereby inherits all the abstract methods of an interface.");
        jPanel1.add(Correcta1, new org.netbeans.lib.awtextra.AbsoluteConstraints(37, 48, -1, -1));

        SegundaIncorrecta4.setBackground(new java.awt.Color(255, 255, 255));
        Pregunta4.add(SegundaIncorrecta4);
        SegundaIncorrecta4.setText("Are the access modifiers where it cannot be inherited by the methods.");
        jPanel1.add(SegundaIncorrecta4, new org.netbeans.lib.awtextra.AbsoluteConstraints(41, 443, -1, -1));

        PrimerIncorrecta1.setBackground(new java.awt.Color(255, 255, 255));
        Pregunta1.add(PrimerIncorrecta1);
        PrimerIncorrecta1.setText("It is a technique used by the compilers and instructs to insert complete body of the function.");
        jPanel1.add(PrimerIncorrecta1, new org.netbeans.lib.awtextra.AbsoluteConstraints(37, 73, -1, -1));

        Correcta5.setBackground(new java.awt.Color(255, 255, 255));
        Pregunta5.add(Correcta5);
        Correcta5.setText("Are the access modifiers where it cannot be inherited by the methods. can also be applied to properties, events and methods. ");
        jPanel1.add(Correcta5, new org.netbeans.lib.awtextra.AbsoluteConstraints(43, 511, -1, -1));

        SegundaIncorrecta1.setBackground(new java.awt.Color(255, 255, 255));
        Pregunta1.add(SegundaIncorrecta1);
        SegundaIncorrecta1.setText("The resources which are not currently used. Finalize method is protected , and it is accessible only through this class or by a derived class.");
        jPanel1.add(SegundaIncorrecta1, new org.netbeans.lib.awtextra.AbsoluteConstraints(37, 98, -1, -1));

        PrimerIncorrecta5.setBackground(new java.awt.Color(255, 255, 255));
        Pregunta5.add(PrimerIncorrecta5);
        PrimerIncorrecta5.setText("This modifier cannot be applied to static members. ");
        jPanel1.add(PrimerIncorrecta5, new org.netbeans.lib.awtextra.AbsoluteConstraints(43, 536, -1, -1));

        Correcta2.setBackground(new java.awt.Color(255, 255, 255));
        Pregunta2.add(Correcta2);
        Correcta2.setText("It is an event that occurs during the execution of a program. can be of any type – Run time exception, Error exceptions. ");
        jPanel1.add(Correcta2, new org.netbeans.lib.awtextra.AbsoluteConstraints(40, 157, -1, -1));

        SegundaIncorrecta5.setBackground(new java.awt.Color(255, 255, 255));
        Pregunta5.add(SegundaIncorrecta5);
        SegundaIncorrecta5.setText("It is a technique used by the compilers and instructs to insert complete body of the function.");
        jPanel1.add(SegundaIncorrecta5, new org.netbeans.lib.awtextra.AbsoluteConstraints(43, 561, -1, -1));

        PrimerIncorrecta2.setBackground(new java.awt.Color(255, 255, 255));
        Pregunta2.add(PrimerIncorrecta2);
        PrimerIncorrecta2.setText("It is a collection of abstract method. If the class implements an inheritance, and then thereby inherits all the abstract methods of an interface.");
        jPanel1.add(PrimerIncorrecta2, new org.netbeans.lib.awtextra.AbsoluteConstraints(40, 182, -1, -1));

        SegundaIncorrecta2.setBackground(new java.awt.Color(255, 255, 255));
        Pregunta2.add(SegundaIncorrecta2);
        SegundaIncorrecta2.setText("It is a technique used by the compilers and instructs to insert complete body of the function.");
        jPanel1.add(SegundaIncorrecta2, new org.netbeans.lib.awtextra.AbsoluteConstraints(40, 207, -1, -1));

        jLabel1.setText("What is an interface?");
        jPanel1.add(jLabel1, new org.netbeans.lib.awtextra.AbsoluteConstraints(18, 29, -1, -1));

        Correcta3.setBackground(new java.awt.Color(255, 255, 255));
        Pregunta3.add(Correcta3);
        Correcta3.setText("It is recognized by a compiler and it cannot be broken down into component elements. ");
        jPanel1.add(Correcta3, new org.netbeans.lib.awtextra.AbsoluteConstraints(41, 275, -1, -1));

        jLabel2.setText("What is exception handling?");
        jPanel1.add(jLabel2, new org.netbeans.lib.awtextra.AbsoluteConstraints(18, 132, -1, -1));

        PrimerIncorrecta3.setBackground(new java.awt.Color(255, 255, 255));
        Pregunta3.add(PrimerIncorrecta3);
        PrimerIncorrecta3.setText("It is a good feature of OOPS , and it shows only the necessary details to the client of an object.");
        jPanel1.add(PrimerIncorrecta3, new org.netbeans.lib.awtextra.AbsoluteConstraints(41, 300, -1, -1));

        jLabel3.setText("What are tokens?");
        jPanel1.add(jLabel3, new org.netbeans.lib.awtextra.AbsoluteConstraints(18, 250, -1, -1));

        SegundaIncorrecta3.setBackground(new java.awt.Color(255, 255, 255));
        Pregunta3.add(SegundaIncorrecta3);
        SegundaIncorrecta3.setText("Are the access modifiers where it cannot be inherited by the methods.");
        jPanel1.add(SegundaIncorrecta3, new org.netbeans.lib.awtextra.AbsoluteConstraints(41, 325, -1, -1));

        jLabel4.setText("What is an abstraction?");
        jPanel1.add(jLabel4, new org.netbeans.lib.awtextra.AbsoluteConstraints(18, 368, -1, -1));

        jButton1.setText("Enviar");
        jButton1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton1ActionPerformed(evt);
            }
        });
        jPanel1.add(jButton1, new org.netbeans.lib.awtextra.AbsoluteConstraints(950, 590, -1, -1));

        jButton2.setText("Enviar IN");
        jButton2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton2ActionPerformed(evt);
            }
        });
        jPanel1.add(jButton2, new org.netbeans.lib.awtextra.AbsoluteConstraints(860, 590, -1, -1));

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, 660, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 46, Short.MAX_VALUE))
        );

        pack();
        setLocationRelativeTo(null);
    }// </editor-fold>//GEN-END:initComponents

    private void jButton1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton1ActionPerformed
        // TODO add your handling code here:
        Exams E = new Exams();

        if (Correcta1.isSelected() || PrimerIncorrecta1.isSelected() || SegundaIncorrecta1.isSelected()) {
            if (Correcta2.isSelected() || PrimerIncorrecta2.isSelected() || SegundaIncorrecta2.isSelected()) {
                if (Correcta3.isSelected() || PrimerIncorrecta3.isSelected() || SegundaIncorrecta3.isSelected()) {
                    if (Correcta4.isSelected() || PrimerIncorrecta4.isSelected() || SegundaIncorrecta4.isSelected()) {
                        if (Correcta5.isSelected() || PrimerIncorrecta5.isSelected() || SegundaIncorrecta5.isSelected()) {
                            if (Correcta1.isSelected()) {
                                i++;
                                if (Correcta2.isSelected()) {
                                    i++;
                                    if (Correcta3.isSelected()) {
                                        i++;
                                        if (Correcta4.isSelected()) {
                                            i++;
                                            if (Correcta5.isSelected()) {
                                                i++;

                                            }
                                        }
                                    }
                                }
                            }
 
                            if (i >= 4) {
                                E.ExamPartial1.setEnabled(false);
                                E.ExamPartial3.setEnabled(false);
                                E.ExamPartial4.setEnabled(true);
                            } else {
                                if (i < 4) {
                                    E.ExamPartial3.setEnabled(false);
                                    E.INPartial3.setEnabled(true);
                                }
                            }
                            E.show();
                            this.setVisible(false);
                        } else {
                            JOptionPane.showMessageDialog(null, "Te falta contestar la pregunta 5", "Error", JOptionPane.INFORMATION_MESSAGE);
                        }

                    } else {
                        JOptionPane.showMessageDialog(null, "Te falta contestar la pregunta 4", "Error", JOptionPane.INFORMATION_MESSAGE);
                    }
                } else {
                    JOptionPane.showMessageDialog(null, "Te falta contestar la pregunta 3", "Error", JOptionPane.INFORMATION_MESSAGE);
                }
            } else {
                JOptionPane.showMessageDialog(null, "Te falta contestar la pregunta 2", "Error", JOptionPane.INFORMATION_MESSAGE);
            }
        } else {
            JOptionPane.showMessageDialog(null, "Te falta contestar la pregunta 1", "Error", JOptionPane.INFORMATION_MESSAGE);
        }
    }//GEN-LAST:event_jButton1ActionPerformed

    private void jButton2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton2ActionPerformed
        // TODO add your handling code here:
        Exams E = new Exams();

        if (Correcta1.isSelected() || PrimerIncorrecta1.isSelected() || SegundaIncorrecta1.isSelected()) {
            if (Correcta2.isSelected() || PrimerIncorrecta2.isSelected() || SegundaIncorrecta2.isSelected()) {
                if (Correcta3.isSelected() || PrimerIncorrecta3.isSelected() || SegundaIncorrecta3.isSelected()) {
                    if (Correcta4.isSelected() || PrimerIncorrecta4.isSelected() || SegundaIncorrecta4.isSelected()) {
                        if (Correcta5.isSelected() || PrimerIncorrecta5.isSelected() || SegundaIncorrecta5.isSelected()) {
                            if (Correcta1.isSelected()) {
                                i++;
                                if (Correcta2.isSelected()) {
                                    i++;
                                    if (Correcta3.isSelected()) {
                                        i++;
                                        if (Correcta4.isSelected()) {
                                            i++;
                                            if (Correcta5.isSelected()) {
                                                i++;

                                            }
                                        }
                                    }
                                }
                            }
                           if (i >= 4) {
                                E.INPartial3.setEnabled(false);
                                E.ExamPartial4.setEnabled(true);
                            } else {
                                if (i < 4) {
                                    E.INPartial3.setEnabled(false);
                                    E.extraButton.setEnabled(true);
                                }
                            }
                            E.show();
                            this.setVisible(false);
                        } else {
                            JOptionPane.showMessageDialog(null, "Te falta contestar la pregunta 5", "Error", JOptionPane.INFORMATION_MESSAGE);
                        }

                    } else {
                        JOptionPane.showMessageDialog(null, "Te falta contestar la pregunta 4", "Error", JOptionPane.INFORMATION_MESSAGE);
                    }
                } else {
                    JOptionPane.showMessageDialog(null, "Te falta contestar la pregunta 3", "Error", JOptionPane.INFORMATION_MESSAGE);
                }
            } else {
                JOptionPane.showMessageDialog(null, "Te falta contestar la pregunta 2", "Error", JOptionPane.INFORMATION_MESSAGE);
            }
        } else {
            JOptionPane.showMessageDialog(null, "Te falta contestar la pregunta 1", "Error", JOptionPane.INFORMATION_MESSAGE);
        }
    }//GEN-LAST:event_jButton2ActionPerformed

//    /**
//     * @param args the command line arguments
//     */
//    public static void main(String args[]) {
//        /* Set the Nimbus look and feel */
//        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
//        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
//         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
//         */
//        try {
//            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
//                if ("Nimbus".equals(info.getName())) {
//                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
//                    break;
//                }
//            }
//        } catch (ClassNotFoundException ex) {
//            java.util.logging.Logger.getLogger(ExamPartial3.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
//        } catch (InstantiationException ex) {
//            java.util.logging.Logger.getLogger(ExamPartial3.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
//        } catch (IllegalAccessException ex) {
//            java.util.logging.Logger.getLogger(ExamPartial3.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
//        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
//            java.util.logging.Logger.getLogger(ExamPartial3.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
//        }
//        //</editor-fold>
//        //</editor-fold>
//
//        /* Create and display the form */
//        java.awt.EventQueue.invokeLater(new Runnable() {
//            public void run() {
//                new ExamPartial3().setVisible(true);
//            }
//        });
//    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JRadioButton Correcta1;
    private javax.swing.JRadioButton Correcta2;
    private javax.swing.JRadioButton Correcta3;
    private javax.swing.JRadioButton Correcta4;
    private javax.swing.JRadioButton Correcta5;
    private javax.swing.ButtonGroup Pregunta1;
    private javax.swing.ButtonGroup Pregunta2;
    private javax.swing.ButtonGroup Pregunta3;
    private javax.swing.ButtonGroup Pregunta4;
    private javax.swing.ButtonGroup Pregunta5;
    private javax.swing.JRadioButton PrimerIncorrecta1;
    private javax.swing.JRadioButton PrimerIncorrecta2;
    private javax.swing.JRadioButton PrimerIncorrecta3;
    private javax.swing.JRadioButton PrimerIncorrecta4;
    private javax.swing.JRadioButton PrimerIncorrecta5;
    private javax.swing.JRadioButton SegundaIncorrecta1;
    private javax.swing.JRadioButton SegundaIncorrecta2;
    private javax.swing.JRadioButton SegundaIncorrecta3;
    private javax.swing.JRadioButton SegundaIncorrecta4;
    private javax.swing.JRadioButton SegundaIncorrecta5;
    public static javax.swing.JButton jButton1;
    public static javax.swing.JButton jButton2;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JPanel jPanel1;
    // End of variables declaration//GEN-END:variables
}
